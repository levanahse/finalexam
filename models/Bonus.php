<?php

namespace app\models;

use Yii;

use \yii\web\IdentityInterface;
	use yii\db\ActiveRecord;
	use yii\behaviors\BlameableBehavior;
	use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bonus".
 *
 * @property integer $Id
 * @property string $userId
 * @property string $reasonId
 * @property integer $amount
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property Bonusreason $reason
 */
class Bonus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'reasonId', 'amount'], 'required'],
            [['amount', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['userId'], 'string', 'max' => 64],
            [['reasonId'], 'string', 'max' => 255],
            [['reasonId'], 'exist', 'skipOnError' => true, 'targetClass' => Bonusreason::className(), 'targetAttribute' => ['reasonId' => 'name']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'userId' => 'User ID',
            'reasonId' => 'Reason ID',
            'amount' => 'Amount',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /*
      @return \yii\db\ActiveQuery
     */
    public function getReason()
    {
        return $this->hasOne(Bonusreason::className(), ['name' => 'reasonId']);
    }
	
	public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }
	
	
	
}
