<?php

namespace app\models;
use yii\helpers\ArrayHelper;

use Yii;

/**
 * This is the model class for table "bonusreason".
 *
 * @property integer $Id
 * @property string $name
 */
class Bonusreason extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonusreason';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 65],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'name' => 'Name',
        ];
    }
	
	public static function getBonusreasons()
	{
		$allBonusreason = self::find()->all();
		$allBonusreasonArray = ArrayHelper::
					map($allBonusreason, 'Id', 'name');
		return $allBonusreasonArray;						
	}
}
