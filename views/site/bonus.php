<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Bonus */
/* @var $form ActiveForm */
?>
<div class="site-bonus">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'userId') ?>
        <?= $form->field($model, 'reasonId') ?>
        <?= $form->field($model, 'amount') ?>
        <?= $form->field($model, 'created_at') ?>
        <?= $form->field($model, 'created_by') ?>
        <?= $form->field($model, 'updated_at') ?>
        <?= $form->field($model, 'updated_by') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-bonus -->
